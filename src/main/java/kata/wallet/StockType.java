package kata.wallet;

public enum StockType {
    PETROLEUM,
    EUR,
    USD;

    public static StockType find(final String name) {
        for (final StockType value : StockType.values()) {
            if (value.name().equalsIgnoreCase(name)) {
                return value;
            }
        }
        throw new IllegalArgumentException("Stocke type not found: " + name);
    }
}
