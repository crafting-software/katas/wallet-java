package kata.wallet;

import com.google.common.base.Objects;

import java.math.BigDecimal;

public final class Price {
    private final Currency currency;
    private final BigDecimal amount;

    public static Price price(final Currency currency, final String amount) {
        return price(currency, new BigDecimal(amount));
    }

    public static Price price(final Currency currency, final double amount) {
        return price(currency, BigDecimal.valueOf(amount));
    }

    public static Price price(final Currency currency, final long amount) {
        return price(currency, BigDecimal.valueOf(amount));
    }

    public static Price price(final Currency currency, final BigDecimal amount) {
        return new Price(currency, amount);
    }

    public static Price price(final String price) {
        final String[] tokens = price.trim().split(" ");
        return price(Currency.valueOf(tokens[0]), tokens[1]);
    }

    public static Price add(final Price left, final Price right) {
        return price(left.currency, left.amount.add(right.amount));
    }

    public static Price mul(final Price left, final double quantity) {
        return price(left.currency, left.amount.multiply(BigDecimal.valueOf(quantity)));
    }

    private Price(Currency currency, BigDecimal amount) {
        this.currency = currency;
        this.amount = amount;
    }

    public Price round() {
        return price(currency, currency.round(amount));
    }

    public String render() {
        return currency + " " + currency.render(amount);
    }

    @Override
    public String toString() {
        return render();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return Objects.equal(currency, price.currency) && Objects.equal(currency.render(amount), currency.render(price.amount));
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(currency, amount);
    }
}
