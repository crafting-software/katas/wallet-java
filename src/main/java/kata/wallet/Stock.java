package kata.wallet;

import static kata.wallet.Price.mul;

public final class Stock {
    private final StockType type;
    private final int quantity;

    public Stock(final StockType type, final int quantity) {
        this.type = type;
        this.quantity = quantity;
    }

    public Price evaluate(final Currency currency, final ValueProvider provider) {
        return mul(provider.value(type, currency), quantity).round();
    }
}
