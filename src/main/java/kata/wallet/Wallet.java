package kata.wallet;

import io.vavr.collection.List;

import static kata.wallet.Price.price;

public final class Wallet {
    private final List<Stock> stocks;

    public Wallet(final List<Stock> stocks) {
        this.stocks = stocks;
    }

    public Price evaluate(final Currency currency, final ValueProvider provider) {
        return stocks.map(s -> s.evaluate(currency, provider)).fold(price(currency, 0), Price::add);
    }
}
