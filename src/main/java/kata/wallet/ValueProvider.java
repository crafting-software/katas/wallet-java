package kata.wallet;

public interface ValueProvider {
    Price value(final StockType type, final Currency currency);
}
