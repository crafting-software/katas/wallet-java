package kata.wallet;


import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import static kata.wallet.FixedValueProvider.values;
import static kata.wallet.Price.price;
import static org.junit.jupiter.api.Assertions.assertEquals;

final class WalletUnitTest {
    @Test
    void empty_wallet_should_be_evaluated_to_zero() {
        assertEquals("EUR 0.00", evaluate(wallet(), "EUR"));
        assertEquals("USD 0.000", evaluate(wallet(), "USD"));
    }

    @Test
    void wallet_should_evaluate_stock_according_to_type_and_currency() {
        assertEquals("EUR 10.00", evaluate(wallet(stock("USD", 20)), values("USD/EUR 0.5"), "EUR"));
        assertEquals("EUR 1.00", evaluate(wallet(stock("EUR", 1)), values("EUR/EUR 1"), "EUR"));
    }

    @Test
    void wallet_should_sum_all_stocks_values() {
        assertEquals("EUR 3.00", evaluate(
                wallet(stock("EUR", 1), stock("EUR", 1), stock("EUR", 1)),
                values("EUR/EUR 1"),
                "EUR"));
    }

    private static Stock stock(final String type, final int quantity) {
        return new Stock(StockType.find(type), quantity);
    }

    private static String evaluate(final Wallet wallet, final String currency) {
        return evaluate(wallet, (__, c) -> price(c, 1), currency);
    }

    private static String evaluate(final Wallet wallet, final ValueProvider provider, final String currency) {
        return wallet.evaluate(Currency.find(currency), provider).render();
    }

    private static Wallet wallet(final Stock... stocks) {
        return new Wallet(List.of(stocks));
    }
}
