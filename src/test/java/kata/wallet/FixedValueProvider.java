package kata.wallet;


import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.collect.Maps.newHashMap;
import static java.lang.Double.parseDouble;
import static kata.wallet.Price.price;

final class FixedValueProvider implements ValueProvider {
    private static final Pattern DEFINITION = Pattern.compile("(\\p{Upper}+)/(\\p{Upper}+)\\s+(.+)");

    private final Map<String, Price> prices = newHashMap();

    static ValueProvider values(final String... values) {
        FixedValueProvider provider = new FixedValueProvider();
        for (String definition : values) {
            Matcher result = DEFINITION.matcher(definition);
            if (result.matches()) {
                provider.add(StockType.valueOf(result.group(1)), Currency.valueOf(result.group(2)), parseDouble(result.group(3)));
            }
        }
        return provider;
    }

    public Price value(final StockType type, final Currency currency) {
        return prices.get(key(type, currency));
    }

    private void add(StockType type, Currency currency, double amount) {
        prices.put(key(type, currency), price(currency, amount));
    }

    private static String key(StockType type, Currency currency) {
        return type + "/" + currency;
    }
}
