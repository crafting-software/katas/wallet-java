package kata.wallet;

import com.google.common.base.Objects;
import io.vavr.test.Property;
import org.junit.jupiter.api.Test;

import static io.vavr.test.Arbitrary.integer;
import static kata.wallet.Currency.EUR;
import static kata.wallet.Price.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

final class PriceUnitTest {
    @Test
    void price_should_equal() {
        Property.def("price should equal")
                .forAll(integer())
                .suchThat((v) -> Objects.equal(price(EUR, v), price(EUR, v)))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void money_should_add() {
        Property.def("price should add")
                .forAll(integer(), integer())
                .suchThat((left, right) -> Objects.equal(price(EUR, left + right), add(price(EUR, left), price(EUR, right))))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void money_should_mul() {
        Property.def("price should mul")
                .forAll(integer(), integer())
                .suchThat((value, quantity) -> Objects.equal(price(EUR, value * quantity), mul(price(EUR, value), quantity)))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void money_should_take_currency_precision() {
        assertEquals("EUR 20.23", price(EUR, 20.234).render());
    }
}
